# Response Tap

---

[TOC]

## Introduction

Register and initialise Response Tap integration.

## Download

* [responseTap-register.js](https://bitbucket.org/mm-global-se/if-responsetap/src/master/src/responseTap-register.js)

* [responseTap-initialize.js](https://bitbucket.org/mm-global-se/if-responsetap/src/master/src/responseTap-initialize.js)

## Ask client for

+ Campaign name

+ Slot variable (e.g `"3"` - should be between 0 and 99)

+ The client will have to add additional code to their Response Tap script for the current integration to work (`"var rTapReady=0; function rTapPostReplacement() {rTapReady=1;}"`) Client should double check with their contacts at Response Tap.


## Deployment instructions

### Content Campaign

+ Ensure that you have Integration Factory plugin deployed on site level and has _order: -10_ ([find out more](https://bitbucket.org/mm-global-se/integration-factory))

+ Create another site script with _order: -5_ and add the code from [responseTap-register.js](https://bitbucket.org/mm-global-se/if-responsetap/src/master/src/responseTap-register.js) into it

+ Create campaign script with _order: > -5_, then customize the code from [responseTap-initialize.js](https://bitbucket.org/mm-global-se/if-responsetap/src/master/src/responseTap-initialize.js) and add into it

### Redirect Campaign

Same process as Content Campaign with the addition of mapping the  integration initialisation script to both generation and redirect pages.

## QA

###Network Tab

After the data is successfully sent to Response Tap you should see a new script requested in Network tab of the developer tools (e.g. Chrome DevTools). The name of this request is `customVariables`. In Headers under Query String Parameters you should be able to see the experience data that you are sending to Response Tap.

![network_tab.png](https://bitbucket.org/repo/Kg9o5M/images/3229373733-network_tab.png)