mmcore.IntegrationFactory.initialize('ResponseTap', {
    campaign: '<Campaign Name>',
    slot: '<Variable Slot Number>', // between 0 and 99
    redirect: false,
    persist: true,
    callback: function () {}
});